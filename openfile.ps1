$string = "Hello Bootcamp!"
$outputFile = (Get-Location).Path + '\hello.txt'

Remove-Item $outputFile
New-Item $outputFile -ItemType file
try{
    $writer = [System.IO.StreamWriter] $outputFile
    $writer.WriteLine($string)
    $writer.close();

    Invoke-Item $outputFile  
}finally{}